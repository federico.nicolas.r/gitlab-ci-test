FROM registry.gitlab.com/federico.nicolas.r/gitlab-ci-test/jboss-base:1.0.0

COPY customization ${JBOSS_HOME}/customization/
COPY modules ${JBOSS_HOME}/modules/

RUN "${JBOSS_HOME}/bin/jboss-cli.sh" --file="${JBOSS_HOME}/customization/commands.cli" 

RUN rm -rf "${JBOSS_HOME}/standalone/configuration/standalone_xml_history/current"

